import setuptools

setuptools.setup(
    name="python_package", # Replace with your own username
    version="0.1.0",
    author="Example Author",
    author_email="author@example.com",
    description="A example package",
    packages=['python_package'],
    install_requires=['pandas>=1.1.0',
                        'numpy>=1.19.1',
                        'scikit-learn>=0.23.2',
                        'imbalanced-learn>=0.7.0',
                        'xgboost>=1.1.1'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)

