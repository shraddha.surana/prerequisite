"""
This script runs the Accident Severity application using a development server.
"""
import numpy as np
from flask import Flask, request, jsonify, render_template
import pickle
from sklearn.ensemble import RandomForestClassifier

app = Flask(__name__,template_folder=r'')









@app.route('/')
def home():

    return render_template('index2.html')





@app.route('/predict',methods=['POST','GET'])
def predict():
    '''
    For rendering results on HTML GUI
    '''
    model = pickle.load(open('model1.pkl','rb'))
    # Considering 3 grams and mimnimum frq as 0
    int_features = [float(x) for x in request.form.values()]
    final_features = [np.array(int_features)]
    prediction = model.predict(final_features)

    output = round(prediction[0], 2)

    return render_template('index2.html', prediction_text='Accident Severity {}'.format(output))

if __name__ == "__main__":
    
    app.run(port=8080)


