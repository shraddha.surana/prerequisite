##Quick Guide to run Web-API:

Install all the dependencies from "requirements.txt"

Open the file : app.py and Run it

After that you will get the URL copy that and paste it in MicrosoftEdge browser.

You will have to give input values to the model and then 'press' the predict button.

You will be shown the Accident Severity.

##Take the below values for reference:

Distance: 0.01
Start_lan: 40.30
Start_long: -75.64
TMC: 201
Time_duration: 130
city_Conshohocken: 0
Pressure: 29.74
Temperature: 57
Humidity: 100
Hour: 14

Result:  Accident Severity is 2.0
