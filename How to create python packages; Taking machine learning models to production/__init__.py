import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from xgboost import XGBClassifier
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix, accuracy_score, log_loss
from sklearn.model_selection import train_test_split
from imblearn.under_sampling import NearMiss
from imblearn.combine import SMOTETomek 


class AllInOne_Package:

    def __init__(self,
            modelName = None,
            predict = True,
            confusionMatrix = False,
            underSampling = True,
            upSampling = False,
            accuracyScore = False,
            split_dataset = True,
            X=None,
            Y=None,
            ):
        self.modelName = modelName,
        self.predict = predict
        self.confusionMatrix = confusionMatrix
        self.accuracyScore = accuracyScore
        self.underSampling = underSampling
        self.upSampling = upSampling
        self.split_dataset = split_dataset
        self.X = X
        self.Y = Y

    def sampling(self):
        if self.underSampling and self.upSampling:
            print("ERROR :- Both Under sampling and Up sampling can not be True")
        if self.underSampling:
            return self.under_Sampling()
        elif self.upSampling:
            return self.up_Sampling()
        else:
            print("ERROR")

    def under_Sampling(self):
        _X_train_dataset,_X_test_dataset,_Y_train_dataset,_Y_test_dataset = self.splitDataset()
        nearMiss = NearMiss()
        _X_train,_Y_train = nearMiss.fit_sample(_X_train_dataset, _Y_train_dataset)
        _X_test,_Y_test = nearMiss.fit_sample(_X_test_dataset, _Y_test_dataset)
        return _X_train,_X_test,_Y_train,_Y_test
    
    def up_Sampling(self):
        _X_train_dataset,_X_test_dataset,_Y_train_dataset,_Y_test_dataset = self.splitDataset()
        smk = SMOTETomek(random_state=42)
        _X_train,_Y_train = smk.fit_sample(_X_train_dataset, _Y_train_dataset)
        _X_test,_Y_test = smk.fit_sample(_X_test_dataset, _Y_test_dataset)
        return _X_train,_X_test,_Y_train,_Y_test
    
    def splitDataset(self):
        return train_test_split(self.X,self.Y,test_size=0.25, random_state=0)

    def model(self):

        model = self.modelName[0]
        _X_train_dataset,_X_test_dataset,_Y_train_dataset,_Y_test_dataset = self.sampling()
        model.fit(_X_train_dataset, _Y_train_dataset)
        predictValue = model.predict(_X_test_dataset)
        
        return self.finalOutput(_Y_test_dataset,predictValue)           
    
    def finalOutput(self,_Y_test_dataset,predictValue):

        if (self.predict and self.confusionMatrix) and self.accuracyScore:
            return [predictValue,confusion_matrix(_Y_test_dataset, predictValue),accuracy_score(_Y_test_dataset,predictValue)]
        elif self.confusionMatrix and self.accuracyScore:
            return [confusion_matrix(_Y_test_dataset, predictValue),accuracy_score(_Y_test_dataset,predictValue)]
        elif self.predict and self.confusionMatrix:
            return [predictValue,confusion_matrix(_Y_test_dataset, predictValue)]
        elif self.predict:
            return predictValue
        elif self.accuracyScore:
            return accuracy_score(_Y_test_dataset,predictValue)
        elif self.confusionMatrix:
            return confusion_matrix(_Y_test_dataset, predictValue)
        else:
            return print("ERROR ")

    
